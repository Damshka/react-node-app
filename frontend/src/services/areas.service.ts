import { getOptions } from "./common.service";

const getAreas = async (searchText: string) => {
  const requestOptions = getOptions({ searchText: searchText});

  return await fetch(`/api/stopArea`, requestOptions)
      .then(res => res.json())
      .then(data => {
        return  data.results.map((city: string) => {return {value: city}}) || [];
      })
    .catch(err => {
      console.log(err);
      throw new Error(err.message);
    })
}

export {getAreas}
