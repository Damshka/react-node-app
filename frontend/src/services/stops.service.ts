import { getOptions } from "./common.service";

const getBusStops = async (areaName: string) => {
  const requestOptions = getOptions({value: areaName });

  return await fetch(`/api/stopName`, requestOptions)
      .then(res => res.json())
      .then(data => {
        return data.results.map((city: string) => {return {value: city}}) || [];
      })
      .catch(err => {
        console.log(err);
        throw new Error(err.message);
      })
}

export { getBusStops }
