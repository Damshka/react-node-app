const getOptions = (requestOptions: any) => {
  return {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(requestOptions)
  }
}

export {getOptions};
