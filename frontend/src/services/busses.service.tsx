import { getOptions } from "./common.service";
import React from "react";

const getBusses = async (stopName: string) => {
  const requestOptions = getOptions({ value: stopName});

  return await fetch(`/api/busses`, requestOptions)
    .then(res => res.json())
    .then(data => {
       return  data.results.map((item: any, index: any) => ({
          key: index,
          number: item.route_short_name,
          name: item.route_long_name,
          schedule: item.route_id
        }
       )) || [];
    })
    .catch(err => {
      console.log(err);
      throw new Error(err.message);
    });
}

const _direction = (route: string) => {
  const r = route.split(" - ");
  return <p>{r[0]} &mdash; {r[r.length - 1]}</p>
}

const getBussSchedule = async (id: string, stopName: string) => {
  const now = new Date();
  const time = parseInt(""+now.getHours()+now.getMinutes()+"00");
  const requestOptions = getOptions({ value: id, stopName: stopName, time: time});

  return await fetch(`/api/latest`, requestOptions)
    .then(res => res.json())
    .then(data => {
      return data.results.map((item: any, index: any) => ({
          key: index,
          arrival_time: item.arrival_time,
          departure_time: item.departure_time,
          tripName: _direction(item.trip_long_name)
        }
      )) || [];
    })
    .catch(err => {
      console.log(err);
      throw new Error(err.message);
    });
}

const getNearest = async (lat: number, long: number) => {
  const requestOptions = getOptions({lat, long});
  return await fetch(`/api/findNearest`, requestOptions)
    .then(res => res.json())
    .then(stops => {
      return {
        stopArea: stops.results[0].stop_area,
        stopName: stops.results[0].stop_name
      } || {}
    })
    .catch(err => {
      console.log(err);
      throw new Error(err.message);
    });
}

export {getBusses, getBussSchedule, getNearest}
