import React, { useEffect, useState } from 'react';
import './App.scss';
import Row from "./components/Grid/Row";
import Col from "./components/Grid/Col";
import Button from "./components/Button/Button";
import Autocomplete from "./components/Autocomplete/Autocomplete";
import { Divider, Spin, Table, TableColumnsType } from "antd";
import { DefaultOptionType } from 'antd/lib/select';
import { IData } from "./inerfaces/data.interface";
import { getAreas } from "./services/areas.service";
import { getBusStops } from "./services/stops.service";
import { getBusses, getBussSchedule, getNearest } from "./services/busses.service";


function App() {
  const [data, setData] = useState<IData | null>();
  const [areaOptions, setAreaOptions] = useState<DefaultOptionType[]>([]);
  const [stopsOptions, setStopsOptions] = useState<DefaultOptionType[]>([]);
  const [activeStep, setActiveSteps] = useState(0);

  const [isAppLoading, setIsAppLoading] = useState(true);
  const [isLoading, setIsLoading] = useState(false);
  const [tableData, setTableData] = useState([]);
  const [scheduleTableData, setScheduleTableData] = useState([]);


  const columns: TableColumnsType<never> | undefined = [
    {
      title: 'Route number',
      dataIndex: 'number',
      key: 'number',
    },
    {
      title: 'Route name',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Schedule',
      key: 'schedule',
      dataIndex: 'schedule',
      render: (_, { schedule }) => (
        <>
          <Button onClick={() => {
            loadNearestSchedule(schedule)}}>View time schedule</Button>
        </>
      )
    }
  ];

  const scheduleColumns: TableColumnsType<never> | undefined = [
    {
      title: 'Arrival time',
      dataIndex: 'arrival_time',
      key: 'arrival_time',
    },
    {
      title: 'Departure time',
      dataIndex: 'departure_time',
      key: 'departure_time',
    },
    {
      title: 'Buss direction',
      dataIndex: 'tripName',
      key: 'tripName'
    },
  ];

  useEffect(() => {
    const geolocation = navigator.geolocation;
    if (geolocation) {
      geolocation.getCurrentPosition(
        (position) => {
          findNearest(position.coords.latitude, position.coords.longitude);
          return;
        },
        (err) => {console.log(err); setIsAppLoading(false)})
    } else {
      setIsAppLoading(false);
    }
  }, []);

  let fetchBussStopsTimeout: any;
  const loadBussStops = async (areaName: string) => {
    clearTimeout(fetchBussStopsTimeout);
    fetchBussStopsTimeout = setTimeout(async () => {
      const busStops = await getBusStops(areaName);
      setStopsOptions(busStops);
    }, 500);
  };

  const onSelect = (selectedData: string, type: keyof IData) => {
    setData({...data, [type]: selectedData});
    if(type === 'stopArea') {
      loadBussStops(selectedData);
    }
  };

  let fetchAreasTimeout: any;
  const loadAreas = async (searchText: string) => {
    clearTimeout(fetchAreasTimeout)
    setData({...data, stopArea: searchText});
    if (!searchText.length) {
      reset('full');
      return;
    }
    fetchAreasTimeout = setTimeout(async () => {
      const areas = await getAreas(searchText);
      console.log(areas);
      setAreaOptions(areas);
    }, 500);

  }

  const loadBusses = async (stopName: string) => {
    setIsLoading(true);
    setTableData([]);
    setScheduleTableData([]);
    const busses = await getBusses(stopName);
    setTableData(busses);
    setActiveSteps(2);
    setIsLoading(false);
  }

  const loadNearestSchedule = async (id: string) => {
    const schedule = await getBussSchedule(id, data?.stopName || '');
    setScheduleTableData(schedule);
  }

  const reset = (key: "full" | "partial" = "partial") => {
    if (key === "full") {
      setData({...data, stopName: '', stopArea: ''});
      setAreaOptions([]);
      setStopsOptions([]);
      setActiveSteps(0);
    }

    setIsLoading(false);
    setTableData([]);
    setScheduleTableData([]);
  }

  const findNearest  = async (lat: number, long: number) => {
    const bussData = await getNearest(lat, long);
    setData({...data, ...bussData});
    setActiveSteps(2);
    setIsAppLoading(false);
    await loadBussStops(bussData.stopArea);
    await  loadBusses(bussData.stopName);
  }

  const renderContent = () => {
    return <>
      <Row className={"row"}>
        <Col className="gutter-row" span={24}>
            <Autocomplete className={'autocomplete mr--10'}
                          value={data?.stopArea}
                          options={!!areaOptions && areaOptions}
                          onSelect={(data: any) => {onSelect(data, 'stopArea'); setActiveSteps(1)}}
                          onSearch={(data: any) => loadAreas(data)}
                          placeholder="Enter stop area, e.g. Narva"/>

            {activeStep > 0 && <Autocomplete className={'autocomplete mr--10'}
                          value={data?.stopName}
                          filterOption
                          options={!!stopsOptions && stopsOptions}
                          onSelect={(data: any) => {onSelect(data, 'stopName'); setActiveSteps(2)}}
                          onSearch={(searchText) => {
                            if(!searchText.length) {
                              setActiveSteps(1);
                            }
                            setTableData([]);
                            setScheduleTableData([]);
                            setData({...data, stopName: searchText})}
            }
                          placeholder="Enter bus stop name"/>}
            {activeStep === 2 && <Button
                loading={isLoading}
                onClick={() => loadBusses(data?.stopName as string)}
                type={'primary'}>Load busses</Button>}
          {(!!data?.stopArea && !!data.stopName) &&
              <Button
                  className={"btn--reset"}
                  onClick={() => reset('full')}
                  type={'primary'}>
                    Reset all data
              </Button>}
        </Col>
      </Row>
        {!!tableData.length && !!data?.stopName &&
          <>
            <Divider orientation="left">Busses</Divider>
            <Row className={'row'} gutter={16}>
              <Col className="gutter-row" span={12}>
                <Table scroll={{y: 300}} dataSource={tableData} columns={columns}/>
              </Col>
              <Col className="gutter-row" span={12}>
                {!!scheduleTableData.length && <>
                  <Table scroll={{y: 300}} dataSource={scheduleTableData} columns={scheduleColumns}/>
                </>}
              </Col>
            </Row>
          </>
        }


    </>
  }

  return (
    <div className="App">
      <main className={"main"}>
        {isAppLoading ?
          <div className={"main__spinner-wrapper"}>
            <Spin size="large"/>
          </div> :
          renderContent()
        }
      </main>
    </div>
  );
}

export default App;
