import { FC } from "react";
import { Col as AntCol, ColProps as AntColProps } from 'antd';

export interface ColProps extends AntColProps {}

const Col: FC<ColProps> = (props) => {
  return <AntCol {...props} />
}

export default Col;
