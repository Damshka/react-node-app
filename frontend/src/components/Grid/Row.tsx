import { FC } from "react";
import { Row as AntRow, RowProps as AntRowProps } from 'antd';

export interface RowProps extends AntRowProps {}

const Row: FC<RowProps> = (props) => {
  return <AntRow {...props} />
}

export default Row;
