import { FC } from "react";
import { AutoComplete as AntAutoComplete } from 'antd';
import { AutoCompleteProps } from "antd/lib/auto-complete";

export interface AutocompleteProps extends AutoCompleteProps {}

const Autocomplete: FC<AutocompleteProps> = (props) => {
  return <AntAutoComplete {...props} />
}

export default Autocomplete;
