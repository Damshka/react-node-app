import { Button as AntBtn } from 'antd';
import { FC } from "react";
import { BaseButtonProps, NativeButtonProps } from "antd/lib/button/button";

export interface ButtonProps extends NativeButtonProps {}

const Button: FC<ButtonProps> = (props) => {
  return <AntBtn {...props} />
}

export default Button;
