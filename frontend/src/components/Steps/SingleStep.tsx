import { FC } from "react";
import { Steps as AntSteps } from 'antd';
import { StepProps as AntStepProps} from "antd/lib/steps";

export interface StepProps extends AntStepProps {}

const { Step } = AntSteps;

const SingleStep: FC<StepProps> = (props) => {
  return <Step {...props} />
}

export  default SingleStep;
