import React, { FC } from "react";
import { Steps as AntSteps } from 'antd';
import { StepsProps as AntStepsProps} from "antd/lib/steps";

export interface StepsProps extends AntStepsProps {
  children: React.ReactNode
}

const Steps: FC<StepsProps> = (props) => {
  return <AntSteps {...props} >
    {props.children}
  </AntSteps>
}

export default Steps;
