const express = require("express");
const bodyParser = require('body-parser');
const cors = require("cors");
const path = require('path');

const PORT = process.env.PORT || 3001;

const app = express();
const dataController = require("./controllers/data");

app.use(express.static(path.resolve(__dirname, '../frontend/build')));
app.use(cors());
app.use(bodyParser.json());

app.post("/api/busses", dataController.getBusses);
app.post("/api/latest", dataController.getLatest);
app.post("/api/schedule", dataController.getSchedule);
app.post("/api/stopArea", dataController.getStopArea);
app.post("/api/stopName", dataController.getStopName);
app.post("/api/findNearest", dataController.getNearest);

app.get('*', (req, res) => {
  res.sendFile(path.resolve(__dirname, '../frontend/build', 'index.html'));
});

app.listen(PORT, () => {
  console.log(`Server listening on ${PORT}`);
});
