const db = require('../utlis/database');

exports.fetchStopArea = (value) => {
  return db.execute(`SELECT * from busstops WHERE stop_area LIKE '${value}%'`);
}

exports.fetchStopsName = (value) => {
  return db.execute(`SELECT stop_name from busstops WHERE stop_area='${value}'`);
}

exports.fetchBussesList = (value) => {
  return db.execute(`SELECT DISTINCT r.route_id, r.route_short_name, r.route_long_name
                          FROM busstops AS bs
                          INNER JOIN stop_times AS st ON bs.stop_id=st.stop_id
                          INNER JOIN trips AS t ON st.trip_id=t.trip_id
                          INNER JOIN routes AS r ON t.route_id=r.route_id
                          WHERE bs.stop_name='${value}'
                          ORDER BY r.route_short_name * 1 ASC, r.route_short_name ASC
                          ;`);
}

exports.fetchClosestArrivals = (routeId, stopName, time) => {
  return db.execute(`
    SELECT st.arrival_time, st.departure_time, t.trip_long_name,
    st.arrival_time - ${time} as diff
    FROM routes AS r
    INNER JOIN trips AS t ON r.route_id=t.route_id
    INNER JOIN stop_times AS st ON t.trip_id=st.trip_id
    INNER JOIN busstops AS bs on st.stop_id=bs.stop_id
    WHERE r.route_id='${routeId}' AND bs.stop_name='${stopName}'
    AND st.arrival_time - ${time} > 0
    ORDER BY st.arrival_time ASC
  `);
}

exports.fetchBussSchedule = (routeId, stopName, limit = 10000) => {

  return db.execute(`SELECT DISTINCT st.arrival_time, st.departure_time, t.trip_long_name
                          FROM routes AS r
                          INNER JOIN trips AS t ON r.route_id=t.route_id
                          INNER JOIN stop_times AS st ON t.trip_id=st.trip_id
                          INNER JOIN busstops AS bs on st.stop_id=bs.stop_id
                          WHERE r.route_id='${routeId}' AND bs.stop_name='${stopName}' 
                          ORDER BY st.arrival_time ASC
                          LIMIT ${limit}
                          `)
}

exports.fetchNearest = (lat, long) => {
  const fixedLat = lat.toString().split(".")[0];
  const fixedLong = long.toString().split(".")[0];
  return db.execute(`SELECT *, POW(stop_lat - ${lat}, 2) + POW(stop_lon - ${long}, 2) as DIF
            FROM busstops 
            WHERE stop_lat LIKE '${fixedLat}.%' AND stop_lon LIKE '${fixedLong}.%'
            ORDER BY DIF
            LIMIT 1`);
}
