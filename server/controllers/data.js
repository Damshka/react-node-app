const {fetchBussesList, fetchClosestArrivals, fetchBussSchedule, fetchStopArea, fetchStopsName, fetchNearest} = require("../data/data");

exports.getBusses = (req, res, next) => {
  const value = req.body.value;
  fetchBussesList(value)
      .then(res => {
        return res[0];
      })
      .then(data => res.json({ results: data }))
      .catch(err => console.log(err));
}

exports.getLatest = (req, res, next) => {
  const value = req.body.value;
  const stopName = req.body.stopName;
  const time = req.body.time;
  fetchClosestArrivals(value, stopName, time)
      .then(async res => {
        const result = res[0].slice(0, 5);
        let additionalData = [];
        if (result.length < 5) {
          const additional = 5 - result.length;
          const add = await fetchBussSchedule(value, stopName, additional)
          additionalData = add[0]
        }
        return [...result, ...additionalData]
      })
      .then(data => res.json({ results: data }))
      .catch(err => console.log(err));
}

exports.getSchedule = (req, res, next) => {
  const value = req.body.value;
  const stopName = req.body.stopName;
  fetchBussSchedule(value, stopName)
      .then(res => res[0])
      .then(data => res.json({ results: data }))
      .catch(err => console.log(err));
}

exports.getStopArea = (req, res, next) => {
  const searchRequest = req.body.searchText;
  fetchStopArea(searchRequest)
      .then(result => {
        results = result[0];
        //remove dupl
        const cities = results.map(busStop =>  busStop.stop_area);
        return [...new Set(cities)]
      })
      .then(data => res.json({results: data }))
      .catch(err => console.log(err));
}

exports.getStopName = (req, res, next) => {
  const value = req.body.value;

  fetchStopsName(value)
      .then(result => {
        results = result[0];
        const busStop = results.map(busStop => busStop.stop_name);
        return [...new Set(busStop)];
      })
      .then(data => res.json({ results: data }))
      .catch(err => console.log(err));
}

exports.getNearest = (req, res, next) => {
  const lat = req.body.lat;
  const long = req.body.long;
  fetchNearest(lat, long)
      .then(res => res[0])
      .then(data => res.json({ results: data }))
      .catch(err => console.log(err));
}
